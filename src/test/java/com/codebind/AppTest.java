package com.codebind;

import static org.junit.Assert.*;

import org.junit.Test;

//public class AppTest {
//
//	@Test
//	public void test() {
////		fail("Not yet implemented");
//	}
//
//}
import junit.framework.Assert;
import junit.framework.TestCase;
public class  AppTest extends TestCase {
    Calculator cal = new Calculator();
    
    public void testAdd() {
        Assert.assertEquals(cal.add(10, 20), 30);
    }
    public void testMultiply() {
        Assert.assertEquals(cal.mul(10, 20), 200);
    }
}